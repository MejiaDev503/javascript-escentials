const btnClass = (stock)=>(stock>=50? "btn-primary":"btn-danger disabled"
)
export default function Card({ name, year, genre, stock=9}) {//otra funcion que me permite destructurar directamente el objeto al pasarse por parametros
    //nota, se puede agregar un valor por defecto si el objeto no tiene la variable
     return `
     <div class="card my-2" style="width: 18rem;">
     <div class="card-body">
       <h5 class="card-title">${name}</h5>
       <p class="card-text">${year } - ${genre}</p>
       <a href="#" class="btn ${btnClass(stock)}">Go somewhere</a>
     </div>
   </div>
     
     `;
   }

// las exportaciones por defecto solo se usan una vez y se podrian renombrar cuando se llame un import
