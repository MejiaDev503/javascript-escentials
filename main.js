// const frutas =["pera","manzana"]
// const animal = {
// color: "azul",
// nombre: "bluuweb",
// trabajando: true,
// };

// //destructuring

// //const {color, nombre, trabajando}= animal;x  

// const {color,...rest} = animal;

// console.log(color);
// console.log(rest);
// // si los objetos tienen la key correspondiente no se necesita que esten en orden

// const [pera, manzana] = frutas
// console.log(pera);
import Card  from "./components/Card";
import {app,spinner} from "./elements";
// const games=[
//   {
//     name: "spiderman",
//     year: 2015,
//     genre:"comics",
//     stock:30,
//   },
//   {
//     name: "The game",
//     year: 2010,
//     genre:"science",
//     stock:50,
//   },
//   {
//     name: "Neverland",
//     year: 2019,
//     genre:"furry",
//     stock:80,
//   },
// ]
spinner.innerHTML = `
<div class="spinner-border text-success" role="status">
  <span class="visually-hidden">Loading...</span>
</div>
`
fetch("data.json")//promesa
.then((res)=>{
  if (!res.ok){
    throw{ok:false,msg: "eooro 404"}//throw se salta hasta el .catch
  }
  return res.json()//json es una funcion de fetch que permite transformar la respuesta en un Json, 
  //al ser una promesa, necesita un .then para progresar
})
.then((data)=>{

  data.forEach(item => {
    app.innerHTML+= Card(item)
  });
})
.catch((err)=>{console.log(err)})
//.finally(()=>{spinner.innerHTML='';})
//const[gameOne,gameTwo, gameThree]= games;//destructurados, el nombre de variables se puede modificar cuando se trata de arrays de objetos

// function Card(game){
//   const {name, year, genre} = game//destructurado del objeto game que se recibe
//}

const fakePromise = new Promise((resolve,reject)=>{
  setTimeout(()=>{
    resolve("hola Mundo")
  },2000)
})


const getGames = async ()=>{//async, para reemplazar fetch y .then
  try {
    const hola = await fakePromise;// si el await falla cae directo al catch 
    console.log(hola);
    const res = await fetch("data.json");//reemplaza el .them
    if (!res.ok){
      throw{ok:false,msg: "eooro 404"}//throw se salta hasta el .catch
    }
    const data = await res.json();
    data.forEach(item => {
      app.innerHTML+= Card(item)
    });
  } catch (error) {
    console.log(error);
  } finally{ spinner.innerHTML=''; }
}
getGames();








// app.innerHTML=Card(gameOne);
// app.innerHTML+=Card(gameTwo);
// app.innerHTML+=Card(gameThree);